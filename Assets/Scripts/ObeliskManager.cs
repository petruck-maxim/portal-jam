﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObeliskManager : MonoBehaviour
{

    public GameObject emptyObelisk;
    public GameObject fullObelisk;

    void PickUpSphere()
    {
        emptyObelisk.SetActive(true);
        fullObelisk.SetActive(false);
    }
}
