﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HornieMaterialSwitch : MonoBehaviour
{
    public Material materialRed;
    public Light pointLight;
    public Color lightColor;

    void PickUpSphere()
    {
        GetComponent<SkinnedMeshRenderer>().material = materialRed;
        pointLight.color = lightColor;
    }
}
