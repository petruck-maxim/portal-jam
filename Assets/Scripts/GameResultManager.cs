﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

enum GameResult
{
    None,
    Defeat,
    Victory
}

enum Levels
{
    Level1,
    Level2,
    Level3
}
public class GameResultManager : MonoBehaviour
{
    public float delayDefeat = 1f;
    public float delayVictory = 1f;
    public CanvasGroup canvas;
    GameResult gameResult = GameResult.None;

    float timer = 0f;

    void Update()
    {
        switch (gameResult)
        {
            case GameResult.Defeat:
                timer += Time.deltaTime;
                canvas.alpha = timer / delayDefeat;
                if (timer >= delayDefeat)
                {
                    SceneManager.LoadScene(SceneManager.GetActiveScene().name);
                }
                break;
            case GameResult.Victory:
                timer += Time.deltaTime;
                canvas.alpha = timer / delayVictory;
                if (timer >= delayVictory)
                {
                    int sceneIndex = SceneManager.GetActiveScene().buildIndex;
                    if (sceneIndex == 2)
                    {
                        Application.Quit();
                        return;
                    }

                    SceneManager.LoadScene(sceneIndex + 1);
                }
                break;
            case GameResult.None:
                break;
        }
    }
    void PlayerDied()
    {
        gameResult = GameResult.Defeat;
    }

    void PortalFixed()
    {
        gameResult = GameResult.Victory;
    }
}
