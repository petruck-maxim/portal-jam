﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class WaypointPatrol : MonoBehaviour
{
    public NavMeshAgent navMeshAgent;
    public Transform[] waypoints;
    public float[] delays;

    Animator m_Animator;
    int currentWaypointIndex;
    float timer = 0f;
    void Start()
    {
        navMeshAgent.enabled = false;
        m_Animator = GetComponent<Animator>();
    }
    void Update()
    {
        if (navMeshAgent.enabled && navMeshAgent.remainingDistance < navMeshAgent.stoppingDistance)
        {
            if (timer >= delays[currentWaypointIndex])
            {
                SetIsIdle(false);
                timer = 0;
                currentWaypointIndex = (currentWaypointIndex + 1) % waypoints.Length;
                navMeshAgent.SetDestination(waypoints[currentWaypointIndex].position);
            }
            else
            {
                timer += Time.deltaTime;
                SetIsIdle(true);
            }
        }
    }
    void PickUpSphere()
    {
        this.StartPatroling();
    }
    void StartPatroling()
    {
        SetIsIdle(false);
        navMeshAgent.enabled = true;
        navMeshAgent.SetDestination(waypoints[0].position);
    }
    void SetIsIdle(bool isIdle)
    {
        m_Animator.SetBool("IsIdle", isIdle);
        m_Animator.SetBool("IsPatroling", !isIdle);
    }
}
