﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FallDetector : MonoBehaviour
{
    public GameObject player;
    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject == player)
        {
            // player has fallen
            player.SendMessage("PlayerDied");
        }
    }
}
