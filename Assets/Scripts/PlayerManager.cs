﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerManager : MonoBehaviour
{
    public float turnSpeed = 20f;
    public float moveSpeed = 20f;
    public GameObject gameManager;
    public GameObject sphere;

    Animator m_Animator;
    Rigidbody m_Rigidbody;
    Vector3 m_Movement;
    Quaternion m_Rotation = Quaternion.identity;

    bool hasSphere = false;

    void Start()
    {
        m_Animator = GetComponent<Animator>();
        m_Rigidbody = GetComponent<Rigidbody>();
    }

    void FixedUpdate()
    {
        float horizontal = Input.GetAxis("Horizontal");
        float vertical = Input.GetAxis("Vertical");


        bool hasHorizontalInput = !Mathf.Approximately(horizontal, 0f);
        bool hasVerticalInput = !Mathf.Approximately(vertical, 0f);
        bool isWalking = hasHorizontalInput || hasVerticalInput;
        m_Animator.SetBool("IsWalking", isWalking);

        Vector3 desiredForward = Vector3.RotateTowards(transform.forward, m_Movement, turnSpeed * Time.deltaTime, 0f);
        m_Rotation = Quaternion.LookRotation(desiredForward);

        m_Movement.Set(horizontal, 0f, vertical);
        m_Movement.Normalize();
        m_Movement *= moveSpeed;
    }

    void OnAnimatorMove()
    {
        m_Rigidbody.MovePosition(m_Rigidbody.position + m_Movement);
        m_Rigidbody.MoveRotation(m_Rotation);
    }

    void PickUpSphere()
    {
        hasSphere = true;
        sphere.SetActive(true);
    }

    void PortalFixed()
    {
        hasSphere = false;
        sphere.SetActive(false);
    }

    void PlayerDied()
    {
        gameManager.SendMessage("PlayerDied");
    }
}