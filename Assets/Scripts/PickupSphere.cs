﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickupSphere : MonoBehaviour
{
    public GameObject player;
    public GameObject enemies;
    public GameObject obelisk;
    public GameObject portal;

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject == player)
        {
            player.SendMessage("PickUpSphere");
            obelisk.SendMessage("PickUpSphere");
            portal.SendMessage("PickUpSphere");
            enemies.BroadcastMessage("PickUpSphere");

            GetComponent<Collider>().enabled = false; // disable collider afterwards
        }
    }
}
