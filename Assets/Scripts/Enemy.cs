﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    public GameObject player;
    Animator m_Animator;
    void Start()
    {
        m_Animator = GetComponent<Animator>();
    }
    void PickUpSphere()
    {
        m_Animator.SetBool("IsSphereGone", true);
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject == player)
        {
            // player was hit
            player.SendMessage("PlayerDied");
        }
    }
}
