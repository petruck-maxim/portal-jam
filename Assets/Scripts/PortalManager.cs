﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PortalManager : MonoBehaviour
{

    public GameObject brokenPortal;
    public GameObject fixedPortal;
    public GameObject player;
    public GameObject gameManager;

    bool playerHasSphere = false;

    void PickUpSphere()
    {
        playerHasSphere = true;
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject == player && playerHasSphere)
        {
            fixedPortal.SetActive(true);
            brokenPortal.SetActive(false);
            gameManager.SendMessage("PortalFixed");
            player.SendMessage("PortalFixed");
            GetComponent<Collider>().enabled = false; // disable collider afterwards
        }
    }
}
